package com.example.cindyng.typertester;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ResultScreen extends AppCompatActivity{

    TextView correctWordsText, wpmText, wrongWordsText, totalWordsText, highestWPM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_screen);

        //initialize the variables with data from the typing screen activity
        int WPM = getIntent().getIntExtra("WORDS_PER_MINUTE", 0);
        int wrongWords = getIntent().getIntExtra("WRONG_WORDS", 0);
        int totalWords = getIntent().getIntExtra("TOTAL_WORDS", 0);
        int highestWPMCount = getIntent().getIntExtra("HIGHEST_SCORE", 0);

        correctWordsText = findViewById(R.id.correctWords);
        wpmText = findViewById(R.id.wpm);
        wrongWordsText = findViewById(R.id.wrongWords);
        totalWordsText = findViewById(R.id.totalWords);
        highestWPM = findViewById(R.id.highestWPM);

        wpmText.setText("WPM: " + WPM);
        correctWordsText.setText("Correct Words: " + WPM);
        wrongWordsText.setText("Wrong Words: " + wrongWords);
        totalWordsText.setText("Total Words: " + totalWords);
        highestWPM.setText("Highest WPM: " + highestWPMCount);

    }

    void retryButton(View v){
        Intent nextScreen = new Intent (v.getContext(), TypingScreen.class);
        startActivity(nextScreen);
    }

    void menuButton(View v){
        Intent nextScreen = new Intent(v.getContext(), MainScreen.class);
        startActivity(nextScreen);
    }


}
