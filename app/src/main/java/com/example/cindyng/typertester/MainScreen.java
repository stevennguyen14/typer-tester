package com.example.cindyng.typertester;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);
    }

    public void PlayButton(View v){
        //Go to the selected screen
        Intent nextScreen = new Intent(v.getContext(), TypingScreen.class);
        startActivity(nextScreen);
    }

    public void ExitButton(View v){
        finish();
        System.exit(0);
    }
}
