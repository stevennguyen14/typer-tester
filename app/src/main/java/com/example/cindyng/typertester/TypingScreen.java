package com.example.cindyng.typertester;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Array;


public class TypingScreen extends AppCompatActivity {

    TextView timerText;
    TextView generatedWords;
    EditText textInput;
    boolean timeAlreadyStarted = false;
    //String coloredWords = "";
    int WPM, wrongWords, totalWords;
    int highestScore;
    int wordIteration = 0;

    String newGeneratedString;

    //declare timer
    CountDownTimer timerCounter = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.typing_screen);

        generatedWords = findViewById(R.id.generatedWords);
        textInput = findViewById(R.id.inputText);

        //set the text view to have scrolling
        final String generatedString = generateWords(150);
        generatedWords.setText(generatedString);
        generatedWords.setMovementMethod(new ScrollingMovementMethod());

        //split the generatedString
        final String[] generatedArray = generatedString.split(" ");

        //disable auto correction
        textInput.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

        textInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //start timer when text changed
                if(!timeAlreadyStarted)
                {
                    startTimer();
                    timeAlreadyStarted = true;
                    newGeneratedString = generatedString;
                }
                //check if user has pressed space
                if(textInput.getText().toString().endsWith(" "))
                {
                    //remove the space at the end of the text
                    String currentText = textInput.getText().toString().replace(" ", "");
                    //remove the first word from the generated strings
                    newGeneratedString = newGeneratedString.substring(newGeneratedString.indexOf(" ") + 1);
                    generatedWords.setText(newGeneratedString);

                    //check if user input matches the highlighted text
                    if(currentText.equals(generatedArray[wordIteration])){
                        //clear out text input box
                        textInput.setText("");

                        //add 1 to WPM counter
                        WPM++;
                        wordIteration++;

                        //change word iteration color
                        //coloredWords += generatedArray[wordIteration];
                        //String uncoloredWords = generatedWords.getText().toString().replaceFirst(generatedArray[wordIteration], " ");

                    }
                    //if user input doesn't match, count one towards wrong words variable
                    else{
                        wrongWords++;
                        wordIteration++;
                        textInput.setText("");

                    }
                    totalWords++;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    String generateWords(int wordCount){
        //library of random words
        String words = "give was think cut see form both took run one question went few boy made day four it move " +
                "just carry let quickly change were this get of both large time world any away show long young next " +
                "even took into high again find each other tell take almost quick plant together story home live know also house " +
                "not but read spell enough after more been answer face watch point book talk work idea rat random fat your mum wheel australia";

        //split the library of words into an array of words
        String[] arrayWords = words.split(" ");
        String generatedWords = "";

        for(int i = 0; i < wordCount; i++){
            //Generate a random number
            int randomNumber = (int) (Math.random() * Array.getLength(arrayWords));
            //assign and add to generated words string with the arraywords using the random number
            generatedWords += arrayWords[randomNumber] + " ";
        }

        return generatedWords;
    }

    public void nextScreen(){
        //Go to the selected screen
        Intent nextScreen = new Intent(TypingScreen.this, ResultScreen.class);
        //feed the variables to the result screen activity
        nextScreen.putExtra("WORDS_PER_MINUTE", WPM);
        nextScreen.putExtra("WRONG_WORDS", wrongWords);
        nextScreen.putExtra("TOTAL_WORDS", totalWords);
        nextScreen.putExtra("HIGHEST_SCORE", highestScore);
        //start the next activity
        startActivity(nextScreen);
    }

    //start timer
    void startTimer(){

        timerText = findViewById(R.id.textTime);

        timerCounter = new CountDownTimer(61000, 1000){

            @Override
            public void onTick(long millisUntilFinished) {
                timerText.setText(millisUntilFinished / 1000 + "s");
            }

            @Override
            public void onFinish() {
                timerText.setText("Time over!");
                //assign highscore to saved highscore
                highestScore = loadScore();
                //check if current score is higher then saved score
                if(WPM > highestScore){
                    //save current score to highest score
                    saveScore(WPM);
                    //assign the new score
                    highestScore = loadScore();
                }
                nextScreen();
            }
        };
        timerCounter.start();
    }


    //cancel timer needs to be called whenever onDestroy()/onDestroyView() in the owning Activity/Fragment is called.
    void cancelTimer(){
        if(timerCounter != null){
            timerCounter.cancel();
        }
    }

    void retryButton(View v){
        //reset the timer and restart it
        cancelTimer();
        startTimer();
        textInput.setText("");
    }

    void saveScore(int wpm){
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("highscore", wpm);
        editor.commit();
    }

    int loadScore(){
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        int highScore = sharedPref.getInt("highscore", 0);
        
        return highScore;
    }

}
